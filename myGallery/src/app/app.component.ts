import { Component, OnInit, ViewChild} from '@angular/core'; 
import { ImageService } from './services/image.service';
import { Image } from './interfaces/image.interface';
import {
  trigger,
  style,
  animate,
  transition,
} from '@angular/animations';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [ 
    trigger(
      'inOutAnimation', 
      [
        transition(
          ':enter', 
          [
            style({ opacity: 0 }),
            animate('1.5s ease-out', 
                    style({  opacity: 1 }))
          ]
        ),
      ]
    )
  ]
})
export class AppComponent implements OnInit {

  @ViewChild('myForm', {static: false}) formVals;
  images: Image[] = [];
  imageOffset: string = "0px 0px 200px 0";
  
  constructor (private _imageService: ImageService) { }

  changeGalSett(galleryConfig: NgForm) : void {
    console.log(galleryConfig.value);
    if(galleryConfig.value.imgNum) {
      this._imageService.fetchImages(galleryConfig.value.imgNum.toString()).subscribe((data: Image[] ) => {
        this.images = data;
      })
    }
    if(galleryConfig.value.windowOffset) {
      this.imageOffset = `0px 0px ${galleryConfig.value.windowOffset}px 0px`;
    }
    this.formVals.resetForm(); 
  } 

  ngOnInit(): void {
    this._imageService.fetchImages("10").subscribe((data: Image[] ) => {
      this.images = data;
    })
    
  }
}

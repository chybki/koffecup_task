import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Image } from './../interfaces/image.interface';
import { API_ADRESS} from './../config/config';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ImageService {

  constructor(private http: HttpClient) { }

  fetchImages(imgNum) : Observable<Image[]> {
    return this.http.get<Image[]>(API_ADRESS, {params: {'_limit': imgNum} })
  }
}

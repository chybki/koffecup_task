import { AfterViewInit, Directive, ElementRef, EventEmitter, OnDestroy, Output, Input } from '@angular/core';
@Directive({
    selector: '[lazyload]'
})
export class LazyLoadDirective implements AfterViewInit, OnDestroy {

    @Output() public lazyload: EventEmitter<any> = new EventEmitter();    
    @Input() public imageOffset: string;
    private _intersectionObserver? : IntersectionObserver;

    constructor (
        private _element: ElementRef
    ) {}

    public ngAfterViewInit () {
      this.registerIntersectionObserver();
      this._intersectionObserver.observe(<Element>(this._element.nativeElement));
    }

    public ngOnDestroy () {
      if (this._intersectionObserver) 
        this._intersectionObserver.disconnect();
    }

    private registerIntersectionObserver (): void {
        if (!!this._intersectionObserver) {
            return;
        }
        this._intersectionObserver = new IntersectionObserver(entries => {
            this.checkForIntersection(entries);
        }, {rootMargin: this.imageOffset});
    }

    private checkForIntersection = (entries: Array<IntersectionObserverEntry>) => {
    entries.forEach((entry: IntersectionObserverEntry) => {
        if (this.checkIfIntersecting(entry)) {
            this.lazyload.emit();
        }
    });
    }

    private checkIfIntersecting (entry: IntersectionObserverEntry) {
    return (<any>entry).isIntersecting && entry.target === this._element.nativeElement;
    }

}